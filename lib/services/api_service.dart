import 'package:dio/dio.dart';
import 'package:majootestcase/models/detail_model.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices {
  final String apiKey = "?api_key=7fa385eed7ac07da7397d62b6533ffc4";

  final String popularMovie = '/movie/popular';
  final String detailMovie = '/movie/';

  Future getMovieList() async {
    try {
      var dio = await dioConfig.dio();

      Response response = await dio!.get(popularMovie + apiKey);

      return MovieModel.fromJson(response.data);
    } on DioError catch (error) {
      if (error.type == DioErrorType.response) {
        print(error.response!.data);
        var errorBody = error.response!.data;
        return errorBody;
      } else if (error.type == DioErrorType.connectTimeout) {
        return 'Koneksi terhenti, Harap periksa internet koneksi anda';
      } else if (error.type == DioErrorType.other) {
        return 'Koneksi terhenti, Harap periksa internet koneksi anda';
      } else {
        return error.response!.data;
      }
    }
  }

  Future getDetailMovie(int idMovie) async {
    try {
      var dio = await dioConfig.dio();

      Response response =
          await dio!.get(detailMovie + idMovie.toString() + apiKey);

      return DetailMovie.fromJson(response.data);
    } on DioError catch (error) {
      if (error.type == DioErrorType.response) {
        print(error.response!.data);
        var errorBody = error.response!.data;
        return errorBody;
      } else if (error.type == DioErrorType.connectTimeout) {
        return 'Koneksi terhenti, Harap periksa internet koneksi anda';
      } else if (error.type == DioErrorType.other) {
        return 'Koneksi terhenti, Harap periksa internet koneksi anda';
      } else {
        return error.response!.data;
      }
    }
  }
}
