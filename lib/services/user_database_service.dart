// ignore_for_file: unused_field

import 'dart:io';
import 'package:majootestcase/models/user.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'package:path_provider/path_provider.dart';

class UserDatabase {
  static String? path;
  static final _databaseName = "testCase.db";
  static final _databaseVersion = 1;

  static final _tableUser = 'users';
  static final _tableLogins = 'logins';

  UserDatabase._privateConstructor();
  static final UserDatabase instance = UserDatabase._privateConstructor();

  static Database? _database;

  Future<Database> get database async => _database ??= await _initDatabase();

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute(
      "CREATE TABLE users(id INTEGER PRIMARY KEY autoincrement, email TEXT, username TEXT, password TEXT)",
    );
    await db.execute(
      "CREATE TABLE logins(email TEXT, username TEXT, password TEXT)",
    );
  }

  static Future getFileData() async {
    return getDatabasesPath().then((s) {
      return path = s;
    });
  }

  Future userRegister(User user) async {
    Database db = await instance.database;

    // var users =
    //     await db.rawQuery("select * from users where email = " + user.email);
    // if (users.length > 0) {
    //   return 'email sudah terdaftar';
    // } else {
    await db.insert("users", user.toUserMap(),
        conflictAlgorithm: ConflictAlgorithm.ignore);
    await db.insert("logins", user.toUserMap(),
        conflictAlgorithm: ConflictAlgorithm.ignore);
    return user;
    // }
  }

  Future checkUserLogin() async {
    Database db = await instance.database;
    List list = [];
    var res = await db.rawQuery("select * from logins");
    if (res.length > 0) {
      list = res.toList().map((c) => User.fromMap(c)).toList();

      print("Data " + list.toString());
      await db.insert("logins", list[0].toUserMap(),
          conflictAlgorithm: ConflictAlgorithm.ignore);
      print(list[0]);
      return list[0];
    } else {
      return 'user not login yet';
    }
  }

  Future userLogin(User user) async {
    Database db = await instance.database;
    var logins =
        await db.rawQuery("select * from users where email = '${user.email}'");
    if (logins.length > 0) {
      await db.insert("logins", user.toUserMap(),
          conflictAlgorithm: ConflictAlgorithm.ignore);
      print('berhasil login dengan data: ${logins[0]}');
      return user;
    } else {
      return 'Login gagal, periksa kembali inputan anda';
    }
  }

  Future getUserData(String email) async {
    Database db = await instance.database;
    var res = await db.rawQuery("select * from logins where email = $email");
    print("result user data $res");
    print("result user data " + res.toString());
    List list = res.toList().map((c) => User.fromMap(c)).toList();
    return list[0];
  }

  Future userLogout(String email) async {
    Database db = await instance.database;
    var logins =
        db.delete(_tableLogins, where: "email = ?", whereArgs: [email]);
    await logins;
    return 'logOut Success';
  }
}
