import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/user_database_service.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void checkUserLogin() async {
    emit(AuthBlocInitialState());
    final result = await UserDatabase.instance.checkUserLogin();
    if (result is User) {
      emit(AuthBlocCheckLoginLoadedState(result));
    } else {
      emit(AuthBlocCheckLoginFailedState(result));
    }
  }

  void loginUser(User user) async {
    final result = await UserDatabase.instance.userLogin(user);
    if (result is User) {
      print('login sukses');
      emit(AuthBlocLoginLoadedState(result));
    } else {
      emit(AuthBlocLoginFailedState(result));
      print('login gagal $result');
      emit(AuthBlocCheckLoginFailedState(result));
    }
  }

  void registrationUser(User user) async {
    emit(AuthBlocLoadingState());
    final result = await UserDatabase.instance.userRegister(user);
    if (result is User) {
      emit(AuthBlocRegisterLoadedState(result));
    } else {
      emit(AuthBlocRegisterFailedState(result));
    }
  }

  void goToRegistrationPage() {
    emit(AuthBlocInitRegisterState());
  }
}
