part of 'auth_bloc_cubit.dart';

abstract class AuthBlocState extends Equatable {
  const AuthBlocState();

  @override
  List<Object> get props => [];
}

class AuthBlocInitialState extends AuthBlocState {}

class AuthBlocLoadingState extends AuthBlocState {}

class AuthBlocInitRegisterState extends AuthBlocState {}

class AuthBlocCheckLoginLoadedState extends AuthBlocState {
  final User data;

  AuthBlocCheckLoginLoadedState(this.data);

  @override
  List<Object> get props => [data];
}

class AuthBlocLoginLoadedState extends AuthBlocState {
  final User data;

  AuthBlocLoginLoadedState(this.data);

  @override
  List<Object> get props => [data];
}

class AuthBlocRegisterLoadedState extends AuthBlocState {
  final User data;

  AuthBlocRegisterLoadedState(this.data);

  @override
  List<Object> get props => [data];
}

class AuthBlocCheckLoginFailedState extends AuthBlocState {
  final String error;

  AuthBlocCheckLoginFailedState(this.error);

  @override
  List<Object> get props => [error];
}

class AuthBlocLoginFailedState extends AuthBlocState {
  final String error;

  AuthBlocLoginFailedState(this.error);

  @override
  List<Object> get props => [error];
}

class AuthBlocRegisterFailedState extends AuthBlocState {
  final String error;

  AuthBlocRegisterFailedState(this.error);

  @override
  List<Object> get props => [error];
}
