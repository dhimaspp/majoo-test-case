import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchingData() async {
    emit(HomeBlocInitialState());
    ApiServices apiServices = ApiServices();
    final movieResponse = await apiServices.getMovieList();
    if (movieResponse is MovieModel) {
      emit(HomeBlocLoadedState(movieResponse.results));
    } else {
      emit(HomeBlocErrorState(movieResponse));
    }
  }
}
