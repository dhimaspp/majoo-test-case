import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/detail_model.dart';

import 'package:majootestcase/services/api_service.dart';

part 'detail_movie_bloc_state.dart';

class DetailMovieBlocCubit extends Cubit<DetailMovieBlocState> {
  DetailMovieBlocCubit() : super(DetailMovieBlocInitialState());

  void fetchingDetailMovieData(int idMovie) async {
    emit(DetailMovieBlocInitialState());
    ApiServices apiServices = ApiServices();
    final detailMovieResponse = await apiServices.getDetailMovie(idMovie);
    if (detailMovieResponse is DetailMovie) {
      emit(DetailMovieBlocLoadedState(detailMovieResponse));
    } else {
      emit(DetailMovieBlocErrorState(detailMovieResponse));
    }
  }
}
