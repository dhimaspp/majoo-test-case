part of 'detail_movie_bloc_cubit.dart';

abstract class DetailMovieBlocState extends Equatable {
  const DetailMovieBlocState();

  @override
  List<Object?> get props => [];
}

class DetailMovieBlocInitialState extends DetailMovieBlocState {}

class DetailMovieBlocLoadingState extends DetailMovieBlocState {}

class DetailMovieBlocLoadedState extends DetailMovieBlocState {
  final DetailMovie? data;
  DetailMovieBlocLoadedState(this.data);

  @override
  List<Object?> get props => [data];
}

class DetailMovieBlocErrorState extends DetailMovieBlocState {
  final error;

  DetailMovieBlocErrorState(this.error);

  @override
  List<Object> get props => [error];
}
