import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/theme/const_theme.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/services/user_database_service.dart';

class SideBarDrawer extends StatelessWidget {
  final String username, email;

  const SideBarDrawer({
    required this.username,
    required this.email,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            padding: const EdgeInsets.only(bottom: 12),
            child: Center(
              child: Text(
                "Majoo \nTest Case",
                style: Theme.of(context).textTheme.headline4!.copyWith(
                    letterSpacing: -4, fontSize: 48, color: Colors.white),
              ),
            ),
            decoration: BoxDecoration(
              color: kMaincolor,
            ),
          ),
          ListTile(
            leading: Icon(
              Icons.exit_to_app,
              color: kSecondaryColor,
            ),
            title: Text('Logout',
                style: Theme.of(context)
                    .textTheme
                    .subtitle1!
                    .copyWith(fontSize: 15)),
            onTap: () async {
              await UserDatabase.instance.userLogout(email);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => BlocProvider(
                            create: (context) =>
                                AuthBlocCubit()..checkUserLogin(),
                            child: MyHomePageScreen(),
                          )));
              print('logout');
            },
          ),
        ],
      ),
    );
  }
}
