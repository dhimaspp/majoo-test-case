import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const kMaincolor = Color(0xFF00adaa);
const kSecondaryColor = Color(0xFF92c86b);
final themeTest = ThemeData(
  textTheme: TextTheme(
      headline1: GoogleFonts.quicksand(),
      subtitle2: GoogleFonts.quicksand(),
      headline2: GoogleFonts.quicksand(),
      headline4: GoogleFonts.quicksand(),
      headline5: GoogleFonts.quicksand(),
      headline6: GoogleFonts.quicksand(),
      overline: GoogleFonts.quicksand(),
      button: GoogleFonts.quicksand(),
      subtitle1: GoogleFonts.quicksand(),
      headline3: GoogleFonts.quicksand(fontWeight: FontWeight.w700),
      bodyText1: GoogleFonts.quicksand(fontWeight: FontWeight.w400),
      bodyText2: GoogleFonts.quicksand(fontWeight: FontWeight.w300),
      caption: GoogleFonts.quicksand(fontWeight: FontWeight.w200)),
  splashColor: kMaincolor,
  primaryColorDark: const Color(0xFF307B52),
  visualDensity: VisualDensity.adaptivePlatformDensity,
  primaryColorLight: const Color(0xFFEAF1ED),
  primaryColor: const Color(0xFF00adaa),
  textSelectionTheme: const TextSelectionThemeData(cursorColor: Colors.black),
  scaffoldBackgroundColor: Colors.white,
  inputDecorationTheme: InputDecorationTheme(
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(8),
    ),
  ),
  colorScheme: ColorScheme.fromSwatch().copyWith(secondary: kMaincolor),
);
