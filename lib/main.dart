import 'package:majootestcase/common/theme/const_theme.dart';
import 'package:majootestcase/ui/auth/login/login_page.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/home_bloc/home_bloc_cubit.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Majoo Test Case',
      theme: themeTest,
      home: BlocProvider(
        create: (context) => AuthBlocCubit()..checkUserLogin(),
        child: MyHomePageScreen(),
      ),
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocCubit, AuthBlocState>(
        buildWhen: (previous, current) =>
            previous != current && current is AuthBlocCheckLoginFailedState ||
            previous != current && current is AuthBlocCheckLoginLoadedState,
        builder: (context, state) {
          print(state);
          if (state is AuthBlocCheckLoginFailedState) {
            return LoginPage();
          } else if (state is AuthBlocCheckLoginLoadedState) {
            return BlocProvider(
              create: (context) => HomeBlocCubit()..fetchingData(),
              child: HomeBlocScreen(
                user: state.data,
              ),
            );
          } else {
            return Scaffold(
              body: Center(
                child: Text(
                  "Majoo Test Case",
                  style: TextStyle(
                      fontSize: 32,
                      color: kMaincolor,
                      fontWeight: FontWeight.bold),
                ),
              ),
            );
          }
        });
  }
}
