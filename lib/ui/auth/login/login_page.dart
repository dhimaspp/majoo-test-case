import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/theme/const_theme.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/auth/register/register_page.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  bool _isLoginFailed = false;
  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  final scaffoldKey = GlobalKey<ScaffoldState>();
  _showMsg(msg) {
    final snackBar = SnackBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      duration: const Duration(milliseconds: 1800),
      padding: const EdgeInsets.only(bottom: 100, left: 80, right: 80),
      content: Container(
          margin: const EdgeInsets.all(12),
          alignment: Alignment.center,
          height: 50,
          decoration: BoxDecoration(
              color: Colors.grey.shade900,
              borderRadius: BorderRadius.circular(12)),
          child: Text(
            msg,
            textAlign: TextAlign.center,
          )),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        showDialog<bool>(
          context: context,
          builder: (c) => AlertDialog(
            title: Text('Perhatian'),
            content: Text(
              'Anda yakin ingin keluar aplikasi?',
            ),
            actions: [
              ElevatedButton(
                child: Text('Ya'),
                onPressed: () => exit(0),
              ),
              ElevatedButton(
                child: Text('Tidak'),
                onPressed: () => Navigator.pop(c, false),
              ),
            ],
          ),
        );
        return true;
      },
      child: Scaffold(
        key: scaffoldKey,
        body: BlocListener<AuthBlocCubit, AuthBlocState>(
          listener: (context, state) {
            print('nge print $state');
            if (state is AuthBlocLoginLoadedState) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => BlocProvider(
                    create: (context) => HomeBlocCubit()..fetchingData(),
                    child: HomeBlocScreen(
                      user: state.data,
                    ),
                  ),
                ),
              );
            } else if (state is AuthBlocLoginFailedState) {
              print('login gagal listener');
              setState(() {
                _isLoginFailed = true;
              });
              _showMsg(state.error);
            } else if (state is AuthBlocInitRegisterState) {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BlocProvider(
                          lazy: false,
                          create: (context) => AuthBlocCubit(),
                          child: RegisterPage())));
            }
          },
          child: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              padding:
                  EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [kSecondaryColor, kMaincolor])),
              child: Align(
                child: Container(
                  constraints: BoxConstraints(maxHeight: 470),
                  padding: EdgeInsets.all(12),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Selamat Datang',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          // color: colorBlue,
                        ),
                      ),
                      Text(
                        'Silahkan login terlebih dahulu',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(
                        height: 9,
                      ),
                      _form(),
                      SizedBox(
                        height: 25,
                      ),
                      CustomButton(
                        text: 'Login',
                        onPressed: handleLogin,
                        height: 100,
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      _isLoginFailed == false
                          ? SizedBox()
                          : Center(
                              child: Container(
                                padding: const EdgeInsets.all(6),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    color: Colors.red.shade50.withOpacity(0.2)),
                                child: Text(
                                  'Login gagal, periksa kembali inputan anda',
                                  style: TextStyle(color: Colors.red, shadows: [
                                    Shadow(color: Colors.black38, blurRadius: 1)
                                  ]),
                                ),
                              ),
                            ),
                      // SizedBox(
                      //   height: 20,
                      // ),
                      _register(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(
                  r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
              if (val == null || val.isEmpty) {
                print('val');
                return "E-mail harus terisi";
              } else {
                return pattern.hasMatch(val)
                    ? null
                    : 'Masukan e-mail yang valid';
              }
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            // validator: (value) => value.isEmpty ?,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
                color: kMaincolor,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          context.read<AuthBlocCubit>().goToRegistrationPage();
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  void handleLogin() async {
    if (formKey.currentState!.validate()) {
      User user = User(_emailController.textController.text, '',
          _passwordController.textController.text);
      context.read<AuthBlocCubit>().loginUser(user);
    }
  }
}
