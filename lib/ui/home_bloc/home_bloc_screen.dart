import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/common/theme/const_theme.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/extra/loading_home.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import 'home_bloc_loaded_screen.dart';
import '../extra/error_screen.dart';

class HomeBlocScreen extends StatelessWidget {
  final User user;
  const HomeBlocScreen({required this.user, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(builder: (context, state) {
      print(state);
      if (state is HomeBlocLoadedState) {
        return HomeBlocLoadedScreen(
          data: state.data,
          user: user,
        );
      } else if (state is HomeBlocInitialState) {
        return LoadingHomePage();
      } else if (state is HomeBlocErrorState) {
        return ErrorScreen(
          message: state.error,
          imageAsset: 'assets/network_error.png',
          retry: () {
            context.read<HomeBlocCubit>().fetchingData();
          },
        );
      }

      return Scaffold(
        body: Center(
          child: Text(
            "Majoo Test Case",
            style: TextStyle(
                fontSize: 32, color: kMaincolor, fontWeight: FontWeight.bold),
          ),
        ),
      );
    });
  }
}
