import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/detail_movie_bloc/detail_movie_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/theme/const_theme.dart';
import 'package:majootestcase/common/widget/side_bar.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/detail_movie/detail_movie_screen.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Results>? data;
  final User user;

  const HomeBlocLoadedScreen({required this.user, Key? key, this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        drawer: SideBarDrawer(
          username: user.userName,
          email: user.email,
        ),
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          title: const Text(
            'Film Sedang Populer',
            style: TextStyle(
                fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
          ),
          backgroundColor: kMaincolor,
        ),
        body: ListView.separated(
          separatorBuilder: (context, index) => Divider(
            indent: 20,
            endIndent: 20,
            thickness: 2,
            color: Colors.black12,
          ),
          itemCount: data!.length,
          itemBuilder: (context, index) {
            return GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => BlocProvider(
                          create: (context) => DetailMovieBlocCubit()
                            ..fetchingDetailMovieData(data![index].id!),
                          child: DetailMovieBlocScreen(
                            indexMovie: data![index].id!,
                          )),
                    ),
                  );
                },
                child: movieItemWidget(data![index]));
          },
        ),
      ),
    );
  }

  Widget movieItemWidget(Results data) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(12),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: CachedNetworkImage(
                imageUrl: "https://image.tmdb.org/t/p/w500/" + data.posterPath!,
                progressIndicatorBuilder: (context, url, downloadProgress) =>
                    LinearProgressIndicator(
                        minHeight: 530,
                        backgroundColor: Colors.grey[100],
                        color: Colors.grey[300],
                        value: downloadProgress.progress),
                errorWidget: (context, url, error) => Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.error,
                      size: 30,
                    ),
                    Text(
                      'Harap periksa koneksi internet anda\nTap ikon refresh dibawah untuk refresh halaman',
                      textAlign: TextAlign.center,
                    ),
                    IconButton(
                        onPressed: () {
                          context.read<HomeBlocCubit>().fetchingData();
                        },
                        icon: const Icon(
                          Icons.refresh_rounded,
                          size: 30,
                        ))
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: Text(data.title!,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                textDirection: TextDirection.ltr),
          )
        ],
      ),
    );
  }
}
