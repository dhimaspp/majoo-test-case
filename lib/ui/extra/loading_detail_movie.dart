import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LoadingDetailMoviePage extends StatefulWidget {
  @override
  _LoadingDetailMoviePageState createState() => _LoadingDetailMoviePageState();
}

class _LoadingDetailMoviePageState extends State<LoadingDetailMoviePage> {
  bool _enabled = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Shimmer.fromColors(
        baseColor: Colors.grey.shade300,
        highlightColor: Colors.grey.shade100,
        enabled: _enabled,
        child: ListView.builder(
          itemBuilder: (_, __) => Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(24),
                        bottomRight: Radius.circular(24))),
                height: 380,
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Container(
                  height: 20,
                  color: Colors.white,
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Container(
                  height: 20,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          itemCount: 6,
        ),
      ),
    );
  }
}
