// ignore_for_file: implementation_imports

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:majootestcase/bloc/detail_movie_bloc/detail_movie_bloc_cubit.dart';
import 'package:majootestcase/models/detail_model.dart';
import 'package:provider/src/provider.dart';

class DetailMovieLoaded extends StatelessWidget {
  final int index;
  final DetailMovie data;
  const DetailMovieLoaded({required this.data, required this.index, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 380,
              child: Stack(
                // fit: StackFit.loose,
                children: [
                  SizedBox(
                    width: double.infinity,
                    height: 380,
                    child: ClipRRect(
                      borderRadius: const BorderRadius.only(
                          bottomLeft: Radius.circular(24),
                          bottomRight: Radius.circular(24)),
                      child: CachedNetworkImage(
                        height: 300,
                        imageUrl: "https://image.tmdb.org/t/p/w500/" +
                            data.posterPath!,
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) =>
                                LinearProgressIndicator(
                                    backgroundColor: Colors.grey[100],
                                    color: Colors.grey[300],
                                    value: downloadProgress.progress),
                        errorWidget: (context, url, error) => Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.error,
                              size: 30,
                            ),
                            Text(
                              'Harap periksa koneksi internet anda\nTap ikon refresh dibawah untuk refresh halaman',
                              textAlign: TextAlign.center,
                            ),
                            IconButton(
                                onPressed: () {
                                  context
                                      .read<DetailMovieBlocCubit>()
                                      .fetchingDetailMovieData(index);
                                },
                                icon: const Icon(
                                  Icons.refresh_rounded,
                                  size: 30,
                                ))
                          ],
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 100,
                        decoration: const BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter,
                                colors: [
                                  Colors.black54,
                                  Colors.black45,
                                  Colors.transparent
                                ]),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(24),
                                bottomRight: Radius.circular(24))),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 18, bottom: 6),
                        child: Text(
                          data.title!,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 18, bottom: 18),
                        child: Text(
                          '${data.releaseDate!.split("-").first} - ${data.genres![0]!.name!}',
                          style: const TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                  GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        height: 30,
                        width: 30,
                        color: Colors.black45,
                        margin: const EdgeInsets.only(left: 18, top: 20),
                        child: const Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        ),
                      ))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Nilai Rating ${data.voteAverage}/10',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 3),
              child: RatingBar.builder(
                ignoreGestures: true,
                itemSize: 20,
                initialRating: data.voteAverage!,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 10,
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (value) {},
              ),
            ),
            // SizedBox(
            //   height: 6,
            // ),
            Divider(
              indent: 8,
              endIndent: 120,
              thickness: 1,
              color: Colors.black38,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Overview',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: Text(
                data.overview!,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
              ),
            ),
            Divider(
              indent: 8,
              endIndent: 120,
              thickness: 1,
              color: Colors.black38,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Popularity ${data.popularity}',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
