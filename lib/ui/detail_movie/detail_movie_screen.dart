import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/detail_movie_bloc/detail_movie_bloc_cubit.dart';
import 'package:majootestcase/common/theme/const_theme.dart';
import 'package:majootestcase/ui/detail_movie/detail_movie_loaded_screen.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';

import 'package:majootestcase/ui/extra/loading_detail_movie.dart';

class DetailMovieBlocScreen extends StatelessWidget {
  final int indexMovie;
  const DetailMovieBlocScreen({required this.indexMovie, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DetailMovieBlocCubit, DetailMovieBlocState>(
        builder: (context, state) {
      if (state is DetailMovieBlocLoadedState) {
        return DetailMovieLoaded(
          data: state.data!,
          index: indexMovie,
        );
      } else if (state is DetailMovieBlocInitialState) {
        return LoadingDetailMoviePage();
      } else if (state is DetailMovieBlocErrorState) {
        return ErrorScreen(
          message: state.error,
          retry: () => context
              .read<DetailMovieBlocCubit>()
              .fetchingDetailMovieData(indexMovie),
        );
      }

      return Scaffold(
        body: Center(
          child: Text(
            "Majoo Test Case",
            style: TextStyle(
                fontSize: 32, color: kMaincolor, fontWeight: FontWeight.bold),
          ),
        ),
      );
    });
  }
}
