class User {
  int? id;
  String email;
  String userName;
  String password;

  User(this.email, this.userName, this.password);

  static fromMap(Map c) {
    return User(c['email'], c['username'], c['password']);
  }

  toUserMap() {
    return {'email': email, 'username': userName, 'password': password};
  }
}
