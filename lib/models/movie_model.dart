class Results {
  String? posterPath;
  bool? adult;
  String? overview;
  String? releaseDate;
  List<int?>? genreIds;
  int? id;
  String? originalTitle;
  String? originalLanguage;
  String? title;
  String? backdropPath;
  double? popularity;
  int? voteCount;
  bool? video;
  double? voteAverage;

  Results({
    this.posterPath,
    this.adult,
    this.overview,
    this.releaseDate,
    this.genreIds,
    this.id,
    this.originalTitle,
    this.originalLanguage,
    this.title,
    this.backdropPath,
    this.popularity,
    this.voteCount,
    this.video,
    this.voteAverage,
  });
  Results.fromJson(Map<String, dynamic> json) {
    posterPath = json['poster_path']?.toString();
    adult = json['adult'];
    overview = json['overview']?.toString();
    releaseDate = json['release_date']?.toString();
    if (json['genre_ids'] != null) {
      final v = json['genre_ids'];
      final arr0 = <int>[];
      v.forEach((v) {
        arr0.add(v.toInt());
      });
      genreIds = arr0;
    }
    id = json['id']?.toInt();
    originalTitle = json['original_title']?.toString();
    originalLanguage = json['original_language']?.toString();
    title = json['title']?.toString();
    backdropPath = json['backdrop_path']?.toString();
    popularity = json['popularity']?.toDouble();
    voteCount = json['vote_count']?.toInt();
    video = json['video'];
    voteAverage = json['vote_average']?.toDouble();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['poster_path'] = posterPath;
    data['adult'] = adult;
    data['overview'] = overview;
    data['release_date'] = releaseDate;
    if (genreIds != null) {
      final v = genreIds;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v);
      });
      data['genre_ids'] = arr0;
    }
    data['id'] = id;
    data['original_title'] = originalTitle;
    data['original_language'] = originalLanguage;
    data['title'] = title;
    data['backdrop_path'] = backdropPath;
    data['popularity'] = popularity;
    data['vote_count'] = voteCount;
    data['video'] = video;
    data['vote_average'] = voteAverage;
    return data;
  }
}

class MovieModel {
  int? page;
  List<Results>? results;
  int? totalResults;
  int? totalPages;

  MovieModel({
    this.page,
    this.results,
    this.totalResults,
    this.totalPages,
  });
  MovieModel.fromJson(Map<String, dynamic> json) {
    page = json['page']?.toInt();
    if (json['results'] != null) {
      final v = json['results'];
      final arr0 = <Results>[];
      v.forEach((v) {
        arr0.add(Results.fromJson(v));
      });
      results = arr0;
    }
    totalResults = json['total_results']?.toInt();
    totalPages = json['total_pages']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['page'] = page;
    if (results != null) {
      final v = results;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['results'] = arr0;
    }
    data['total_results'] = totalResults;
    data['total_pages'] = totalPages;
    return data;
  }
}





// class MovieModel {
//   int? page;
//   List<Results>? results;
//   int? totalPages;
//   int? totalResults;

//   MovieModel({this.page, this.results, this.totalPages, this.totalResults});

//   MovieModel.fromJson(Map<String, dynamic> json) {
//     page = json['page'];
//     if (json['results'] != null) {
//       results = <Results>[];
//       json['results'].forEach((v) {
//         results!.add(new Results.fromJson(v));
//       });
//     }
//     totalPages = json['total_pages'];
//     totalResults = json['total_results'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['page'] = this.page;
//     if (this.results != null) {
//       data['results'] = this.results!.map((v) => v.toJson()).toList();
//     }
//     data['total_pages'] = this.totalPages;
//     data['total_results'] = this.totalResults;
//     return data;
//   }
// }

// class Results {
//   double? voteAverage;
//   String? overview;
//   String? releaseDate;
//   bool? adult;
//   String? backdropPath;
//   int? voteCount;
//   List<int>? genreIds;
//   int? id;
//   String? originalLanguage;
//   String? originalTitle;
//   String? posterPath;
//   String? title;
//   bool? video;
//   double? popularity;
//   String? mediaType;
//   String? name;
//   String? originalName;
//   // List<String>? originCountry;
//   String? firstAirDate;

//   Results(
//       {this.voteAverage,
//       this.overview,
//       this.releaseDate,
//       this.adult,
//       this.backdropPath,
//       this.voteCount,
//       this.genreIds,
//       this.id,
//       this.originalLanguage,
//       this.originalTitle,
//       this.posterPath,
//       this.title,
//       this.video,
//       this.popularity,
//       this.mediaType,
//       this.name,
//       this.originalName,
//       // this.originCountry,
//       this.firstAirDate});

//   Results.fromJson(Map<String, dynamic> json) {
//     voteAverage = json['vote_average'];
//     overview = json['overview'];
//     releaseDate = json['release_date'];
//     adult = json['adult'];
//     backdropPath = json['backdrop_path'];
//     voteCount = json['vote_count'];
//     genreIds = json['genre_ids'].cast<int>();
//     id = json['id'];
//     originalLanguage = json['original_language'];
//     originalTitle = json['original_title'];
//     posterPath = json['poster_path'];
//     title = json['title'];
//     video = json['video'];
//     popularity = json['popularity'];
//     mediaType = json['media_type'];
//     name = json['name'];
//     originalName = json['original_name'];
//     // originCountry = json['origin_country'].cast<String>();
//     firstAirDate = json['first_air_date'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['vote_average'] = this.voteAverage;
//     data['overview'] = this.overview;
//     data['release_date'] = this.releaseDate;
//     data['adult'] = this.adult;
//     data['backdrop_path'] = this.backdropPath;
//     data['vote_count'] = this.voteCount;
//     data['genre_ids'] = this.genreIds;
//     data['id'] = this.id;
//     data['original_language'] = this.originalLanguage;
//     data['original_title'] = this.originalTitle;
//     data['poster_path'] = this.posterPath;
//     data['title'] = this.title;
//     data['video'] = this.video;
//     data['popularity'] = this.popularity;
//     data['media_type'] = this.mediaType;
//     data['name'] = this.name;
//     data['original_name'] = this.originalName;
//     // data['origin_country'] = this.originCountry;
//     data['first_air_date'] = this.firstAirDate;
//     return data;
//   }
// }
